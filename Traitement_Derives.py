# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import matplotlib.pyplot as plt
import pylab as py

'-----------------------------------------------------------------------------'

'############## PARTIE 1 : RECUPERATION DES DONNEES DE DERIVE ################'

#### Partie 1.A : Fichier d'acquisition en format .txt chargé en mémoire #####

"""
/!\ Nécessite qu'il n'y ait que des chiffres et non des lettres dans le 
fichier, et pas de dates mais uniquement le temps en sec (formatage préalable).
Préciser le nom et le chemin du fichier s'il n'est pas dans le même répertoire
que le programme de traitement.
"""

data = np.loadtxt("Micmag_220921_MA27cbresil_TGZ_40microns_1.txt")
# On retire la valeur du premier temps pour commencer à zéro
data[:,24] -= data[0,24]

# On affiche le nb de lignes = nb de pts d'acquisition du scan
print("")
print("Le scan comporte", len(data), "points d'acquisition,")

# On récupère le nb de lignes du tableau de données du scan
nb_lignes_data=np.size(data,0)
# Puis le nb de colonnes
nb_colonnes_data=np.size(data,1)

#---------------------------------------------------------------------------

##### Partie 1.B :Tableau de données pour les dérives et graphes ###########

# On compte le nb d'acquisitions à (0;0), la position de référence
# utilisée pour caractériser la dérive

nb_lignes_derive=0
for i in range(nb_lignes_data):
# si (x;y) = (0;0) incrémenter la variable comptant le nb de pts pr la dérive
    if (data[i,2]==0) and (data[i,4]==0):
        nb_lignes_derive=nb_lignes_derive+1

print("dont", nb_lignes_derive,"points de référence pour le traitement de la dérive")
print("")

# Definition du tableau qui va enregistrer les données de dérive
# avec pour dimensions le nombre de lignes de dérive compté avant
# et le nombre de colonnes du fichier initial de données du scan
derive=np.empty((nb_lignes_derive, nb_colonnes_data))
                
# Remplissage du tableau de dérive avec les données du scan aux points (0;0)
j=0
for i in range(nb_lignes_data):
#si (x;y) = (0;0) copier ces lignes dans le tableau de dérive d'indice j
    if (data[i,2]==0) and (data[i,4]==0):
        derive[j,:]=data[i,:]
        j=j+1
        
#---------------------------------------------------------------------------

############# Partie 1.C : Affichage des graphiques de dérive #############

# Récupération des temps correspondant aux points de dérive
temps_derive=derive[:,24]

# Récupération des tensions des capteurs aux points de dérive
# x 1000 pour des valeurs en mV. Puis affichage des graphes respectifs.
plt.figure()
val_derive_V1P=derive[:, 8]*1000
plt.title("Dérive des capteurs proches de la face 1 (V1P)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V1P (mV)")
plt.plot(temps_derive,val_derive_V1P, marker="o", color="blue")

plt.figure()
val_derive_V1L=derive[:, 10]*1000
plt.title("Dérive des capteurs loin de la face 1 (V1L)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V1L (mV)")
plt.plot(temps_derive,val_derive_V1L, marker="o", color="grey")

plt.figure()
val_derive_V2P=derive[:, 12]*1000
plt.title("Dérive des capteurs proches de la face 2 (V2P)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V2P (mV)")
plt.plot(temps_derive,val_derive_V2P, marker="o", color="blue")

plt.figure()
val_derive_V2L=derive[:, 14]*1000
plt.title("Dérive des capteurs loin de la face 2 (V2L)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V2L (mV)")
plt.plot(temps_derive,val_derive_V2L, marker="o", color="grey")

plt.figure()
val_derive_V3P=derive[:, 16]*1000
plt.title("Dérive des capteurs proches de la face 3 (V3P)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V3P (mV)")
plt.plot(temps_derive,val_derive_V3P, marker="o", color="blue")

plt.figure()
val_derive_V3L=derive[:, 18]*1000
plt.title("Dérive des capteurs loin de la face 3 (V3L)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V3L (mV)")
plt.plot(temps_derive,val_derive_V3L, marker="o", color="grey")

plt.figure()
val_derive_V4P=derive[:, 20]*1000
plt.title("Dérive des capteurs proches de la face 4 (V4P)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V4P (mV)")
plt.plot(temps_derive,val_derive_V4P, marker="o", color="blue")

plt.figure()
val_derive_V4L=derive[:, 22]*1000
plt.title("Dérive des capteurs loin de la face 4 (V4L)")
plt.xlabel("Temps (sec)")
plt.ylabel("Tension V4L (mV)")
plt.plot(temps_derive,val_derive_V4L, marker="o",color="grey")
plt.show()

#---------------------------------------------------------------------------

########### Partie 1.D : Plage de dérive des capteurs ###############

### Sous-fonction pour caractériser rapidement les dérives
def carac_derive(num_capteur, proche):
    max_derive=max(derive[:,num_capteur])*1000
    min_derive=min(derive[:,num_capteur])*1000
    amp_derive=max_derive-min_derive
    if (proche == 1):
        max_val = max(data[:,num_capteur])*1000
        min_val = min(data[:, num_capteur])*1000
        amp_val = max_val - min_val
        err_derive = (amp_derive / amp_val) *100
        return [max_derive, min_derive, amp_derive, max_val, min_val, amp_val,
                err_derive]
    elif (proche == 0) :
        return [max_derive, min_derive, amp_derive]
    
print("")   
print("--- Capteurs proches ---")
print("")

# Rq : affichage en "%.3f" %(val) pour n'avoir que 3 chiffres décimaux (+ propre)

## Pour le capteur proche V1P
carac_derive_V1P = carac_derive(8,1)
print("Dérive V1P, valeur max : ", "%.3f" %(carac_derive_V1P[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V1P[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V1P[2]), "mV")
print("Scan V1P, valeur max : ", "%.3f" %(carac_derive_V1P[3]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V1P[4]), "mV | amplitude : ", "%.3f" %(carac_derive_V1P[5]), "mV")
print("Erreur engendrée par la dérive de V1P : ", "%.3f" %(carac_derive_V1P[6]), "%" )
print("  ")

## Pour le capteur proche V2P
carac_derive_V2P = carac_derive(12,1)
print("Dérive V2P, valeur max : ", "%.3f" %(carac_derive_V2P[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V2P[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V2P[2]), "mV")
print("Scan V2P, valeur max : ", "%.3f" %(carac_derive_V2P[3]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V2P[4]), "mV | amplitude : ", "%.3f" %(carac_derive_V2P[5]), "mV")
print("Erreur engendrée par la dérive de V2P : ", "%.3f" %(carac_derive_V2P[6]), "%" )
print("  ")

## Pour le capteur proche V3P
carac_derive_V3P = carac_derive(16,1)
print("Dérive V3P, valeur max : ", "%.3f" %(carac_derive_V3P[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V3P[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V3P[2]), "mV")
print("Scan V3P, valeur max : ", "%.3f" %(carac_derive_V3P[3]), "mV | valeur min : ", 
      "%.3f" %(carac_derive_V3P[4]), "mV | amplitude : ", "%.3f" %(carac_derive_V3P[5]), "mV")
print("Erreur engendrée par la dérive de V3P : ", "%.3f" %(carac_derive_V3P[6]), "%" )
print(" ")

## Pour le capteur proche V4P
carac_derive_V4P = carac_derive(20,1)
print("Dérive V4P, valeur max : ", "%.3f" %(carac_derive_V4P[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V4P[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V4P[2]), "mV")
print("Scan V4P, valeur max : ", "%.3f" %(carac_derive_V4P[3]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V4P[4]), "mV | amplitude : ", "%.3f" %(carac_derive_V4P[5]), "mV")
print("Erreur engendrée par la dérive de V4P : ", "%.3f" %(carac_derive_V4P[6]), "%" )
print(" ")

print("--- Capteurs loin ---")
print("")

## Pour le capteur loin V1L
carac_derive_V1L = carac_derive(10,0)
print("Dérive V1L, valeur max : ", "%.3f" %(carac_derive_V1L[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V1L[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V1L[2]), "mV")
print(" ")

## Pour le capteur loin V2L
carac_derive_V2L = carac_derive(14,0)
print("Dérive V2L, valeur max : ", "%.3f" %(carac_derive_V2L[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V2L[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V2L[2]), "mV")
print(" ")

## Pour le capteur loin V3L
carac_derive_V3L = carac_derive(18,0)
print("Dérive V3L, valeur max : ", "%.3f" %(carac_derive_V3L[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V3L[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V3L[2]), "mV")
print(" ")

## Pour le capteur loin V4L
carac_derive_V4L = carac_derive(22,0)
print("Dérive V4L, valeur max : ", "%.3f" %(carac_derive_V4L[0]), "mV | valeur min : ",
      "%.3f" %(carac_derive_V4L[1]), "mV | amplitude : ", "%.3f" %(carac_derive_V4L[2]), "mV")
print(" ")


'----------------------------------------------------------------------------'

'################### PARTIE 2 : CALCUL DES CORRECTIONS ######################'

# Sous-fonction de calcul des corrections basée sur la méthode point par point

def corr_derive(num_capteur, ind_col_capteur):
    print("A partir de quel point traiter la dérive pour le capteur proche n°",
          num_capteur, "? (voir le graphe associé pour choisir)")
    pt_start_derive = int(input(" "))
    corr_derive=np.empty((nb_lignes_derive-pt_start_derive, 3))
    
    # Tab de correction avec un nb de lignes dépendant du pt de ref de derive 
    # choisi : 1re col. (ind.0) = coeff.directeurs ; 2e col. (ind.1) = écarts
    # par rapport au point de ref de dérive et 3e col. (ind.2) = temps associé
    
    # Stockage des temps des coeffs et des écarts pour la correction de dérive
    for k in range(nb_lignes_derive-pt_start_derive):
        corr_derive[k, 2]= derive[pt_start_derive+k, 24]
        
    # Calcul des coefficients (c.f. méthode point par point) et stockage
    corr_derive[0,0]=0.0
    for k in range(nb_lignes_derive-pt_start_derive-1):
            corr_derive[k+1,0]=(derive[pt_start_derive+1+k, ind_col_capteur]
                                - derive[pt_start_derive+k, ind_col_capteur])/(
                                    derive[pt_start_derive+1+k, 24]-
                                    derive[pt_start_derive+k, 24])
                    
    # Calcul des écarts (c.f. méthode point par point) et stockage
    corr_derive[0,1]=0.0
    corr_derive[1,1]=0.0
    for k in range(nb_lignes_derive-pt_start_derive-2):
        corr_derive[k+2,1]=(derive[pt_start_derive+1+k, ind_col_capteur]-
                            derive[pt_start_derive, ind_col_capteur])    
        
    return corr_derive

# L'appel de la fonction corr_derive pour chaque capteur proche
# renvoie un tableau de correction associé à chaque capteur proche qui
# sera ensuite appliqué pour corriger les données du capteur proche associé

corr_derive_V1P = corr_derive(1, 8)
corr_derive_V2P = corr_derive(2, 12)
corr_derive_V3P = corr_derive(3, 16)
corr_derive_V4P = corr_derive(4, 20)

"""
Vérification du code avec V2P traité de la même façon sous Excel
voir fichier "4e_essai_derive_V2P" à l'emplacement D:/Ni_Labview/Stages/Rachel
/Résultats_tests_prog/Tests_Prog/Nouvelle_sonde_S8/Traitement_derive/
4e_essai_calibration_2h30/Traitement_derive

Fichier à mettre en entrée du programme si l'on veut procédér à cette vérif.

print(corr_derive_V2P[:,0])
print(corr_derive_V2P[:,1])
print(corr_derive_V2P[:,2])

"""

'----------------------------------------------------------------------------'

'############### PARTIE 3 : APPLICATION DES CORRECTIONS #####################'

# Sous-fonction d'application des corrections pour un capteur proche qui sort
# un tab. de données avec la colonne corrigée correspondant au capteur en entrée

def application_corr(donnees, nb_lignes_donnees, ind_col_capteur, correc_derive):
    donnees_corr=donnees.copy()
    j=0
    for i in range(nb_lignes_donnees):
        #si on se situe ds l'intervalle de tps entre 2 pts de la corr. associée
        if (donnees[i,24] >= correc_derive[j,2]) and (donnees[i,24] <= correc_derive[j+1,2]):
        #valeur_corr= valeur - coeff_corr(j+1)*(tempsvaleur-temps(j)) - corr_ecart
           donnees_corr[i,ind_col_capteur]=donnees[i,ind_col_capteur]-correc_derive[(j+1),0]*(
               donnees[i,24]-correc_derive[j,2])-correc_derive[j+1,1]
           
           # Si on n'est pas à la fin du fichier
           # Ici, subtilité avec le -2 : parce qu'on utilise l'ind. j+1 ds le for
           if (i<nb_lignes_donnees-1) and j <(np.size(correc_derive,0)-2):
               # Si on a dépassé l'intervalle de temps associé à une correction
               if (donnees[i+1,24]>correc_derive[j+1,2]):
               # On incrémente j pour passer à la correction suivante dans le tableau
                   j=j+1
 
    return donnees_corr

# On applique cette sous-fonction pour chaque capteur, le tableau de données avec
# toutes les colonnes de capteurs corrigées correspond au dernier tableau data_corr
# Rq : cette méthode n'est peut-être pas optimale car elle utilise en sortie le même tableau
# d'entrée donc si on appelle plusieurs fois cette fonction pour un capteur, les valeurs sont
# modifiées et la correction n'est alors plus correcte. Mais tel quel (un seul appel pour chaque
# capteur) le dernier tableau data_corr est fiable vis-à-vis de la méthode de correction demandée

# Correction pour V1P
data_corr=application_corr(data, nb_lignes_data, 8, corr_derive_V1P)

# Correction pour V2P
data_corr=application_corr(data_corr, nb_lignes_data, 12, corr_derive_V2P)

# Correction pour V3P
data_corr=application_corr(data_corr, nb_lignes_data, 16, corr_derive_V3P)

# Correction pour V4P
data_corr=application_corr(data_corr, nb_lignes_data, 20, corr_derive_V4P)


'----------------------------------------------------------------------------'

'########################### FIN DU PROGRAMME ###############################'

# Enregistrement du tableau de données corrigées dans un fichier

# Sans entête et "%f" pour enregistrer des nombres décimaux
np.savetxt('testastrid.txt', data_corr, fmt='%f')

"""                                        
# Avec entête
np.savetxt('4e_essai_corr.txt', data_corr,fmt='%f',
           header='numero_du_point ligne position_x encodeur_x position_y encodeur_y position_z'
                  ' encodeur_z V1P ecart-type_V1P V1L ecart-type_V1L V2P ecart-type_V2P V2L'
                  ' ecart-type_V2L V3P ecart-type_V3P V3L ecart-type_V3L V4P ecart-type_V4P V4L'
                  ' ecart-type_V4L Temps_(sec)')
# pb du '#" généré dans la 1re ligne de texte dans le fichier texte                          
"""