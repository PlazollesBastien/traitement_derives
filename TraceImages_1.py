# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 09:26:52 2022

@author: adminloc
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata


#read the configure_Traitement.txt file to get all informations
def readConfigFile(configFile):

    with open(configFile, 'r') as f:
        for line in f:
            rLine="".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'input_dir':
                    inputDir = eval(rLine[1])
                elif rLine[0] == 'input_file':
                    inputFile = eval(rLine[1])
                elif rLine[0] == 'header':
                    header = eval(rLine[1])
                    assert type(header) is int
                elif rLine[0] == 'format':
                    method = eval(rLine[1])
                elif rLine[0] == 'output_dir':
                    outputDir = eval(rLine[1])
                elif  rLine[0] == 'ouput_file':
                    outputFile = eval(rLine[1])
                elif rLine[0] == "display":
                    display = eval(rLine[1])
                    assert type(display) is bool

    return inputDir, inputFile, header, method, outputDir, outputFile, display


#Function to display corrected data
def display_corrected_data(data, vecMesure, iDebInit, incEx,idX,idY):
    # Coordonnées x (position de la ligne)
    X = data[57:-58, idX] # !BP : Ici les valeurs représentent le nombre de points de la dernière ligne qui ne sont pas corrigés par défaut dans le code de Rachel et qu'il faut enlever pour générer l'image
                        # Modifier aussi ligne 80
    # Coordonnées y (position du point d'acquisition le long d'une ligne)
    Y = data[57:-58, idY]

    # Partie qui permet d'avoir les pas en x et en y sans ouvrir le fichier param ;)
    # Tant qu'on est sur la première ligne d'indice 0 on passe au point suivant
    # jusqu'à avoir un X différent et la différence de X donne le pas de X
    i = 0
    while (data[i, idX] == data[0,idX]):
        i = i + 1

    pasx = data[i, idX] - data[i - 1, idX]
    pasy = data[0, idY] - data[1, idY]

    print(pasx, pasy)
    # print(pasx)
    # print(pasy)

    # On s'assure qu'on n'a pas de pas négatif
    # (problématique pour tracer les images avec les fonctions utilisées)
    if (pasx < 0):
        pasx = -pasx
    if (pasy < 0):
        pasy = -pasy

    '########### 2) Réalisation des grilles pour tracer les images ############'

    # Grille de coordonnées commune à tous les capteurs
    xx, yy = np.meshgrid(np.arange(np.min(X), np.max(X) + pasx, pasx), np.arange(np.min(Y), np.max(Y) + pasy, pasy))

    xlim = (X.min(), X.max())
    ylim = (Y.min(), Y.max())

    # Données des tensions des capteurs proches
    iDeb = iDebInit
    for i in range(len(vecMesure)):
        Mesure = data[57:-58, iDeb]
        gridMesure =griddata((np.column_stack((X, Y))), Mesure, (xx, yy), method='nearest')
        fig = plt.figure()
        ax = fig.add_subplot(111)
        im = plt.imshow(gridMesure, origin='lower', cmap='gist_earth', extent=xlim + ylim, aspect='auto',
                    interpolation='none')
        plt.colorbar(im, orientation='vertical')
        plt.xlim(xlim)
        ax.set_xlabel('Axe-X (Lignes parcourues)')
        ax.set_ylabel('Axe-Y (Points des lignes)')
        plt.title('Capteur proche n°'+str(i+1)+' (Signal '+vecMesure[i]+')')
        plt.show()

        iDeb += incEx





if __name__ == "__main__":
    '########## 1) Récupération des données servant à tracer les images ##########'

    # Read parameters from configuration file
    inputDir, inputFile, header, method, outputDir, outputFile, display = readConfigFile("configure_Traitement.txt")

    # Fichier de données (ne doit contenir que des chiffres et non des lettres)
    # Changer le nom du fichier selon qu'il s'agit de données brutes ou corrigées
    data = np.genfromtxt(outputDir+'/'+outputFile)

    if method == 'GMR':
        vecMesure = ['V1P', 'V1L', 'V2P', 'V2L', 'V3P', 'V3L', 'V4P', 'V4L']
        iDebInit = 8
        incEx = 2
        idX = 2
        idY = 4
    elif method == 'HALL':
        vecMesure = ['Signal Agilent','Signal Agilent Raz',	'X scan', 'X scan Raz', 'Y scan', 'Y scan Raz', 'R scan', 'R scan Raz']
        iDebInit = 4
        incEx    = 1
        idX = 2
        idY = 3

    display_corrected_data(data, vecMesure, iDebInit, incEx, idX, idY)
