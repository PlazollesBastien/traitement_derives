# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 15:25:36 2022

@author: adminloc
"""

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("V2P.txt")

fig = plt.figure()
ax = fig.add_subplot(111)
im = plt.imshow(data, origin='lower',  aspect='auto', interpolation='none')
#pour d'autres couleurs, voir la palette de couleurs associée à imageshow

plt.colorbar(im, orientation='vertical')
ax.set_xlabel('Axe-X (Lignes parcourues)')
ax.set_ylabel('Axe-Y (Points des lignes)')
plt.title ('Image capteur proche')
plt.show()
