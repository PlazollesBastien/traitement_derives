import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import time
from TraceImages_1 import *

#Lambda to convert decimal separator in Hall files
foo = lambda astr: float(astr.replace(b',',b'.'))

#read the configure_Traitement.txt file to get all informations
def readConfigFile(configFile):

    with open(configFile, 'r') as f:
        for line in f:
            rLine="".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'input_dir':
                    inputDir = eval(rLine[1])
                elif rLine[0] == 'input_file':
                    inputFile = eval(rLine[1])
                elif rLine[0] == 'header':
                    header = eval(rLine[1])
                    assert type(header) is int
                elif rLine[0] == 'format':
                    method = eval(rLine[1])
                elif rLine[0] == 'output_dir':
                    outputDir = eval(rLine[1])
                elif  rLine[0] == 'ouput_file':
                    outputFile = eval(rLine[1])
                elif rLine[0] == "display":
                    display = eval(rLine[1])
                    assert type(display) is bool

    return inputDir, inputFile, header, method, outputDir, outputFile, display


### Sous-fonction pour caractériser rapidement les dérives
def carac_derive(num_capteur, proche,derive):
    max_derive=max(derive[:,num_capteur])*1000
    min_derive=min(derive[:,num_capteur])*1000
    amp_derive=max_derive-min_derive
    if (proche == 1):
        max_val = max(data[:,num_capteur])*1000
        min_val = min(data[:, num_capteur])*1000
        amp_val = max_val - min_val
        err_derive = (amp_derive / amp_val) *100
        return [max_derive, min_derive, amp_derive, max_val, min_val, amp_val,
                err_derive]
    elif (proche == 0) :
        return [max_derive, min_derive, amp_derive]


# Sous-fonction de calcul des corrections basée sur la méthode point par point
def corr_derive(derive,num_capteur, ind_col_capteur, idTime):
    print("A partir de quel point traiter la dérive pour le capteur proche n°",
          num_capteur, "? (voir le graphe associé pour choisir)")
    pt_start_derive = int(input(" "))
    corr_derive=np.empty((nb_lignes_derive-pt_start_derive, 3))

    # Tab de correction avec un nb de lignes dépendant du pt de ref de derive
    # choisi : 1re col. (ind.0) = coeff.directeurs ; 2e col. (ind.1) = écarts
    # par rapport au point de ref de dérive et 3e col. (ind.2) = temps associé

    # Stockage des temps des coeffs et des écarts pour la correction de dérive
    for k in range(nb_lignes_derive-pt_start_derive):
        corr_derive[k, 2]= derive[pt_start_derive+k, idTime]

    # Calcul des coefficients (c.f. méthode point par point) et stockage
    corr_derive[0,0]=0.0
    for k in range(nb_lignes_derive-pt_start_derive-1):
            corr_derive[k+1,0]=(derive[pt_start_derive+1+k, ind_col_capteur]
                                - derive[pt_start_derive+k, ind_col_capteur])/(
                                    derive[pt_start_derive+1+k, idTime]-
                                    derive[pt_start_derive+k, idTime])

    # Calcul des écarts (c.f. méthode point par point) et stockage
    corr_derive[0,1]=0.0
    corr_derive[1,1]=0.0
    for k in range(nb_lignes_derive-pt_start_derive-2):
        corr_derive[k+2,1]=(derive[pt_start_derive+1+k, ind_col_capteur]-
                            derive[pt_start_derive, ind_col_capteur])

    return corr_derive

# Sous-fonction d'application des corrections pour un capteur proche qui sort
# un tab. de données avec la colonne corrigée correspondant au capteur en entrée
def application_corr(donnees, nb_lignes_donnees, ind_col_capteur, correc_derive, idTime):
    donnees_corr=donnees.copy()
    j=0
    for i in range(nb_lignes_donnees-1):
        #si on se situe ds l'intervalle de tps entre 2 pts de la corr. associée
        if (donnees[i,idTime] >= correc_derive[j,2]) and (donnees[i,idTime] <= correc_derive[j+1,2]):
        #valeur_corr= valeur - coeff_corr(j+1)*(tempsvaleur-temps(j)) - corr_ecart
           donnees_corr[i,ind_col_capteur]=donnees[i,ind_col_capteur]-correc_derive[(j+1),0]*(
               donnees[i,idTime]-correc_derive[j,2])-correc_derive[j+1,1]

           # Si on n'est pas à la fin du fichier
           # Ici, subtilité avec le -2 : parce qu'on utilise l'ind. j+1 ds le for
           if (i<nb_lignes_donnees-1) and j <(np.size(correc_derive,0)-2):
               # Si on a dépassé l'intervalle de temps associé à une correction
               if (donnees[i+1,idTime]>correc_derive[j+1,2]):
               # On incrémente j pour passer à la correction suivante dans le tableau
                   j=j+1

    return donnees_corr

# Function to load GMR data from file
def readGMRMagFile(inputDir, inputFile, header):

    if header == 0:
        data = np.genfromtxt(inputDir + '/' + inputFile)
    else:
        dataTemp = np.genfromtxt(inputDir + '/' + inputFile, skip_header=header,usecols=range(24) )
        dateStr = np.genfromtxt(inputDir + '/' + inputFile, skip_header=header,usecols=(25,26), dtype="|U10", autostrip=True)
        #First ugly solution, bit of brute force but it works
        date = np.array([])
        for i in range(len(dateStr)):
            tempo = dateStr[i][0]+' '+ dateStr[i][1]
            t = datetime.strptime(tempo, "%d/%m/%Y %H:%M:%S")
            elapsed = time.mktime(t.timetuple())
            date = np.append(date, elapsed)
        data = np.column_stack((dataTemp,date))


    # On retire la valeur du premier temps pour commencer à zéro
    print(data[0,:])
    data[:, 24] -= data[0, 24]


    return data

# Function to load Hall data from file
def readHallMagFile(inputDir, inputFile, header):

    #Here we reorganise columns to have all the measures one after the other
    data = np.genfromtxt(inputDir + '/' + inputFile, skip_header=header,usecols=(0,1,3,2,4,5,6,7,8,9,10,11), converters={i:foo for i in range(12)})
    #data = np.genfromtxt(inputDir + '/' + inputFile, skip_header=header, usecols=(0, 1, 5, 4, 2, 3, 6, 7, 8), converters={i: foo for i in range(9)})

    return data



def printDerive(timeDerive, value, label, col):

    plt.figure()
    val_derive = value[:] * 1000
    plt.title("Dérive des capteurs proches de la face "+label[1]+" ("+label+")")
    plt.xlabel("Temps (sec)")
    plt.ylabel("Tension "+label+" (mV)")
    plt.plot(timeDerive, val_derive, marker="o", color=col)


if __name__ == "__main__":

    #Read parameters from configuration file
    inputDir, inputFile, header, method, outputDir, outputFile, display = readConfigFile("configure_Traitement.txt")


    '############## PARTIE 1 : RECUPERATION DES DONNEES DE DERIVE ################'

    #### Partie 1.A : Fichier d'acquisition en format .txt chargé en mémoire #####
    if method == 'GMR':
        data = readGMRMagFile(inputDir, inputFile, header)
        vecMesure = ['V1P', 'V1L', 'V2P', 'V2L', 'V3P', 'V3L', 'V4P', 'V4L']
        iDebInit = 8
        incEx    = 2
        idX = 2
        idY = 4
        idTime = 24
    elif method == 'HALL':
        data = readHallMagFile(inputDir, inputFile, header)
        vecMesure = ['Signal Agilent','Signal Agilent Raz',	'X scan', 'X scan Raz', 'Y scan', 'Y scan Raz', 'R scan', 'R scan Raz']
        #vecMesure = ['Signal Agilent', 'Signal Agilent Raz', 'X scan', 'Y scan', 'R scan']
        iDebInit = 4
        incEx    = 1
        idX = 2
        idY = 3
        idTime = 0


    # On affiche le nb de lignes = nb de pts d'acquisition du scan
    print("")
    print("Le scan comporte", len(data), "points d'acquisition,")

    # On récupère le nb de lignes du tableau de données du scan
    nb_lignes_data = np.size(data, 0)
    # Puis le nb de colonnes
    nb_colonnes_data = np.size(data, 1)

    # ---------------------------------------------------------------------------

    ##### Partie 1.B :Tableau de données pour les dérives et graphes ###########

    # On compte le nb d'acquisitions à (0;0), la position de référence
    # utilisée pour caractériser la dérive

    nb_lignes_derive = 0
    for i in range(nb_lignes_data):
        # si (x;y) = (x0;y0) incrémenter la variable comptant le nb de pts pr la dérive
        # !!! BP !!La position diffère d'un fichier à l'autre
        if (data[i, idX] == data[0,idX]) and (data[i, idY] == data[0,idY]):
            nb_lignes_derive = nb_lignes_derive + 1

    print("dont", nb_lignes_derive, "points de référence pour le traitement de la dérive")
    print("")

    # Definition du tableau qui va enregistrer les données de dérive
    # avec pour dimensions le nombre de lignes de dérive compté avant
    # et le nombre de colonnes du fichier initial de données du scan
    derive = np.empty((nb_lignes_derive, nb_colonnes_data))

    # Remplissage du tableau de dérive avec les données du scan aux points (0;0)
    j = 0
    for i in range(nb_lignes_data):
        # si (x;y) = (0;0) copier ces lignes dans le tableau de dérive d'indice j
        if (data[i, idX] == data[0,idX]) and (data[i, idY] == data[0,idY]):
            derive[j, :] = data[i, :]
            j = j + 1

    # ---------------------------------------------------------------------------

    ############# Partie 1.C : Affichage des graphiques de dérive #############

    # Récupération des temps correspondant aux points de dérive
    temps_derive = derive[:, idTime]


    # Récupération des tensions des capteurs aux points de dérive
    # x 1000 pour des valeurs en mV. Puis affichage des graphes respectifs.
    vecCol = ['blue','grey']
    iDeb = iDebInit
    for i in range(len(vecMesure)):
        printDerive(temps_derive, derive[:, iDeb], vecMesure[i], vecCol[i%2])
        iDeb+=incEx

    plt.show()


    # ---------------------------------------------------------------------------

    ########### Partie 1.D : Plage de dérive des capteurs ###############
    #print("")
    #print("--- Capteurs proches ---")
    #print("")

    # Rq : affichage en "%.3f" %(val) pour n'avoir que 3 chiffres décimaux (+ propre)
    iDeb = iDebInit
    corr_deriveMes = []
    for i in range(len(vecMesure)):
        caracDerivCapt = carac_derive(iDeb,1,derive)

        print("Dérive "+vecMesure[i]+", valeur max : ", "%.3f" % (caracDerivCapt[0]), "mV | valeur min : ",
              "%.3f" % (caracDerivCapt[1]), "mV | amplitude : ", "%.3f" % (caracDerivCapt[2]), "mV")
        print("Scan "+vecMesure[i]+", valeur max : ", "%.3f" % (caracDerivCapt[3]), "mV | valeur min : ",
              "%.3f" % (caracDerivCapt[4]), "mV | amplitude : ", "%.3f" % (caracDerivCapt[5]), "mV")
        print("Erreur engendrée par la dérive de "+vecMesure[i]+" : ", "%.3f" % (caracDerivCapt[6]), "%")
        print("  ")

        corr_deriveMes.append(corr_derive(derive,i,iDeb,idTime))
        iDeb += incEx



    '----------------------------------------------------------------------------'

    '################### PARTIE 2 : CALCUL DES CORRECTIONS ######################'
    # L'appel de la fonction corr_derive pour chaque capteur proche
    # renvoie un tableau de correction associé à chaque capteur proche qui
    # sera ensuite appliqué pour corriger les données du capteur proche associé

    #corr_derive_V1P = corr_derive(1, 8)
    #corr_derive_V2P = corr_derive(2, 12)
    #corr_derive_V3P = corr_derive(3, 16)
    #corr_derive_V4P = corr_derive(4, 20)

    '----------------------------------------------------------------------------'

    '############### PARTIE 3 : APPLICATION DES CORRECTIONS #####################'

    # On applique cette sous-fonction pour chaque capteur, le tableau de données avec
    # toutes les colonnes de capteurs corrigées correspond au dernier tableau data_corr
    # Rq : cette méthode n'est peut-être pas optimale car elle utilise en sortie le même tableau
    # d'entrée donc si on appelle plusieurs fois cette fonction pour un capteur, les valeurs sont
    # modifiées et la correction n'est alors plus correcte. Mais tel quel (un seul appel pour chaque
    # capteur) le dernier tableau data_corr est fiable vis-à-vis de la méthode de correction demandée

    iDeb = iDebInit
    data_corr = application_corr(data, nb_lignes_data, iDeb, corr_deriveMes[0],idTime)
    for i in range(len(vecMesure)-1):
        data_corr = application_corr(data_corr, nb_lignes_data, iDeb+incEx, corr_deriveMes[i+1],idTime)
        iDeb += incEx

    # Correction pour V1P
    #data_corr = application_corr(data, nb_lignes_data, 8, corr_derive_V1P)

    # Correction pour V2P
    #data_corr = application_corr(data_corr, nb_lignes_data, 12, corr_derive_V2P)

    # Correction pour V3P
    #data_corr = application_corr(data_corr, nb_lignes_data, 16, corr_derive_V3P)

    # Correction pour V4P
    #data_corr = application_corr(data_corr, nb_lignes_data, 20, corr_derive_V4P)

    '----------------------------------------------------------------------------'

    '########################### FIN DU PROGRAMME ###############################'

    # Enregistrement du tableau de données corrigées dans un fichier

    print("coucou")
    # Sans entête et "%f" pour enregistrer des nombres décimaux
    np.savetxt(outputDir+'/'+outputFile, data_corr, fmt='%f')

    """
    # Avec entête
    np.savetxt('4e_essai_corr.txt', data_corr,fmt='%f',
               header='numero_du_point ligne position_x encodeur_x position_y encodeur_y position_z'
                      ' encodeur_z V1P ecart-type_V1P V1L ecart-type_V1L V2P ecart-type_V2P V2L'
                      ' ecart-type_V2L V3P ecart-type_V3P V3L ecart-type_V3L V4P ecart-type_V4P V4L'
                      ' ecart-type_V4L Temps_(sec)')
    # pb du '#" généré dans la 1re ligne de texte dans le fichier texte                          
    """
    if display :
        display_corrected_data(data_corr, vecMesure, iDebInit, incEx,idX,idY)





