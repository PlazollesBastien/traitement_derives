# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 09:26:52 2022

@author: adminloc
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata

'########## 1) Récupération des données servant à tracer les images ##########'

# Fichier de données (ne doit contenir que des chiffres et non des lettres)
# Changer le nom du fichier selon qu'il s'agit de données brutes ou corrigées
data = np.loadtxt("testastrid.txt")

# Coordonnées x (position de la ligne)
X=data[:,2]
# Coordonnées y (position du point d'acquisition le long d'une ligne)
Y=data[:,4]

# Données des tensions des capteurs proches
V1P = data [:, 8]
V2P = data [:, 12]
V3P = data [:, 16]
V4P = data [:, 20]

# Partie qui permet d'avoir les pas en x et en y sans ouvrir le fichier param ;)
# Tant qu'on est sur la première ligne d'indice 0 on passe au point suivant
# jusqu'à avoir un X différent et la différence de X donne le pas de X
i=0
while (data[i,2]==0):
    i=i+1

pasx=data[i,2]-data[i-1,2]
pasy=data[0,4]-data[1,4]

# print(pasx)
# print(pasy)

# On s'assure qu'on n'a pas de pas négatif
# (problématique pour tracer les images avec les fonctions utilisées)
if (pasx < 0):
    pasx=-pasx
if (pasy < 0):
    pasy=-pasy


'########### 2) Réalisation des grilles pour tracer les images ############'

# Grille de coordonnées commune à tous les capteurs
xx,yy = np.meshgrid(np.arange(np.min(X), np.max(X)+pasx, pasx) , np.arange(np.min(Y), np.max(Y)+pasy, pasy))

xlim = (X.min(), X.max())
ylim = (Y.min(), Y.max())

# Grille de données pour chaque capteur basée sur la grille de coordonnées

grid_V1P = griddata((np.column_stack((X,Y))), V1P, (xx, yy), method='nearest')
grid_V2P = griddata((np.column_stack((X,Y))), V2P, (xx, yy), method='nearest')
grid_V3P = griddata((np.column_stack((X,Y))), V3P, (xx, yy), method='nearest')
grid_V4P = griddata((np.column_stack((X,Y))), V4P, (xx, yy), method='nearest')


'############### 3) Plot pour chaque capteur proche #######################'

# Remarque 1 : L'image est sans interpolation (c.f. les options de imshow)
# changer l'argument "interpolation=  "
# Remarque 2 : Pour d'autres couleurs, c.f. la palette de couleurs associée à imshow
# changer l'argument "cmap=  " comme 'rainbow' par exemple

# Plot pour V1P

fig = plt.figure()
ax = fig.add_subplot(111)
im = plt.imshow(grid_V1P, origin='lower', cmap='gist_earth', extent=xlim+ylim, aspect='auto', interpolation='none')
#im = plt.imshow(grid_V2P, origin='lower', cmap='rainbow', extent=xlim+ylim, aspect='auto', interpolation='none')
#pour d'autres couleurs, voir la palette de couleurs associée à imageshow

plt.colorbar(im, orientation='vertical')
plt.xlim(xlim)
ax.set_xlabel('Axe-X (Lignes parcourues)')
ax.set_ylabel('Axe-Y (Points des lignes)')
plt.title ('Capteur proche n°1 (Signal V1P)')
plt.show()

# Plot pour V2P

fig = plt.figure()
ax = fig.add_subplot(111)
im = plt.imshow(grid_V2P, origin='lower', cmap='gist_earth', extent=xlim+ylim, aspect='auto', interpolation='none')
#im = plt.imshow(grid_V2P, origin='lower', cmap='rainbow', extent=xlim+ylim, aspect='auto', interpolation='none')

plt.colorbar(im, orientation='vertical')
plt.xlim(xlim)
ax.set_xlabel('Axe-X (Lignes parcourues)')
ax.set_ylabel('Axe-Y (Points des lignes)')
plt.title ('Capteur proche n°2 (Signal V2P)')
plt.show()

# Plot pour V3P

fig = plt.figure()
ax = fig.add_subplot(111)
im = plt.imshow(grid_V3P, origin='lower', cmap='gist_earth', extent=xlim+ylim, aspect='auto', interpolation='none')

plt.colorbar(im, orientation='vertical')
plt.xlim(xlim)
ax.set_xlabel('Axe-X (Lignes parcourues)')
ax.set_ylabel('Axe-Y (Points des lignes)')
plt.title ('Capteur proche n°3 (Signal V3P)')
plt.show()

# Plot pour V4P

fig = plt.figure()
ax = fig.add_subplot(111)
im = plt.imshow(grid_V4P, origin='lower', cmap='gist_earth', extent=xlim+ylim, aspect='auto', interpolation='none')

plt.colorbar(im, orientation='vertical')
plt.xlim(xlim)
ax.set_xlabel('Axe-X (Lignes parcourues)')
ax.set_ylabel('Axe-Y (Points des lignes)')
plt.title ('Capteur proche n°4 (TSignal V4P)')
plt.show()